<?php 

session_start();
if(isset($_REQUEST['db']))
	{
		$db=$_REQUEST['db'];
		$_SESSION['db']=$db;
	}
$db=$_SESSION['db'];
include '../loadlang.php';

?>
<html>
  <head>

    <link href="../files/themes/redmond/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
	<meta http-equiv='Content-Type' content='Type=text/html; charset=utf-8'>

	
	<script src="../files/scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="../files/scripts/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
    <script src="../files/scripts/jtable/jquery.jtable.js" type="text/javascript"></script>
	<?php
	echo '<script src="../files/scripts/jtable/localization/'.$lang.'.js" type="text/javascript"></script>
    <link href="../files/scripts/jtable/themes/metro/lightgray/jtable'.$langDir.'.css" rel="stylesheet" type="text/css" />';
	?>
  </head>
<body style="background: url(../files/images/bgnoise_lg.png) repeat left top;">
  <div class="filtering">
    <form>
       <input type="text" name="search" id="search" />
       
        <input type="submit" id="LoadRecordsButton">
    </form>
</div>
	<div id="PeopleTableContainer" style="width: 900px;"></div>
	 <input type="button" id="DeleteAllButton"  value="Delete All Selected Rows"/>
	 <script type="text/javascript">
	//initial input form
	 $('#DeleteAllButton').val(deleteAll);
	 $('#search').attr("placeholder", search);
	 $('#LoadRecordsButton').val(submit_search);
	</script>
	<script type="text/javascript">

		$(document).ready(function () {

		    //Prepare jTable
			$('#PeopleTableContainer').jtable({
				title: 'טבלת לקוחות',
				 messages: Messages, //Lozalize
			  paging: true, //Enable paging
            pageSize: 5, //Set page size (default: 10)
            sorting: true, //Enable sorting
            defaultSorting: 'customer_id ASC', //Set default sorting
            selecting: true, //Enable selecting
            multiselect: true, //Allow multiple selecting
            selectingCheckboxes: true, //Show checkboxes on first column
			
           columnResizable: true, //Disable column resizing
            columnSelectable: true, //Disable column selecting
            saveUserPreferences: true, //Actually, no need to set true since it's default
          
            openChildAsAccordion: true,

            //selectOnRowClick: false, //Enable this to only select using checkboxes

				actions: {
					listAction: 'listActions.php?action=list&db=<?php echo $db; ?>',
					createAction: 'listActions.php?action=create&db=<?php echo $db; ?>',
					updateAction: 'listActions.php?action=update&db=<?php echo $db; ?>',
					deleteAction: 'listActions.php?action=delete&db=<?php echo $db; ?>'
				},
				fields: {
					id: {
						title: numID,
						key: true,
						create: false,
						edit: false,
						list: false,
						
					},
					customer_id: {
						title: customerID,
						width: '14%',
						edit: true,
						sorting:true,
						input: function (data) {
        if (data.record) {
            return '<input type="text" readonly="yes" name="customer_id" style="width:40px" value="' + data.record.customer_id + '" />';
        } else {
		
		 
		 return '<input  type="text" name="customer_id"  style="width:40px"  />';

        }
    }
					},
					name: {
						title: name,
						width: '10%',
						edit: true,
						create: true,
					},
					
					
					phone: {
						title: phone,
						width: '20%',
						
						create: true,
						edit: true,
					},
					adrs: {
						title: adrs,
						width: '40%',
						list:false,
						create: true,
						
						edit: true,
					},
					
					
					email: {
						title: email,
						width: '30%',
						
						create: true,
						edit: true,
					},
					
					precent_down: {
						title: precent_down,
						width: '30%',
						
						create: true,
						edit: true,
					}
				
					
					
					
					
				}
			});
			
			//Load person list from server
			$('#PeopleTableContainer').jtable('load');
			 $('#DeleteAllButton').button().click(function () {
            var $selectedRows = $('#PeopleTableContainer').jtable('selectedRows');
            $('#PeopleTableContainer').jtable('deleteRows', $selectedRows);
        });
		//Re-load records when user click 'load records' button.
        $('#LoadRecordsButton').click(function (e) {
            e.preventDefault();
            $('#PeopleTableContainer').jtable('load', {
                search: $('#search').val(),
                
            });
        });

        //Load all records when page is first shown
        $('#LoadRecordsButton').click();
		$('#PeopleTableContainer').jtable('option', 'pageSize', 10);
		
		// this is for the content of the table
			var fontSize = parseInt($('.jtable').css("font-size"));
			
			fontSize = "12";
			$('.jtable').css({'font-size':fontSize});
			// this is for the headers of the table
			var fontSizetHead= parseInt($('th').css("font-size"));
		
			fontSizetHead = "12";
			$('th').css({'font-size':fontSizetHead});
		
		
		
		
		
		});

	</script>

  </body>
</html>
