﻿ //Localization texts
        var Messages = {
            serverCommunicationError: 'תקלה : אירעה שגיאה בהתחברות לשרת',
            loadingMessage: 'טוען נתונים...',
            noDataAvailable: 'אין נתונים!',
            addNewRecord: 'חדש',
            editRecord: 'ערוך',
            areYouSure: 'האם אתה בטוח?',
            deleteConfirmation: 'האם אתה בטוח שאתה רוצה למחוק?',
            save: 'שמור',
            saving: 'שומר',
            cancel: 'בטל',
            deleteText: 'מחק',
            deleting: 'מוחק',
            error: 'תקלה',
            close: 'סגור',
            cannotLoadOptionsFor: '{0}לא יכול לטעון נתונים עבור  ',
            pagingInfo: 'מציג {0} - {1} , סה"כ {2}',
            canNotDeletedRecords: 'מספר הרישום של {0} {1} לא יימחק!',
            deleteProggress: '{1} {0} מספר ההשמעה נמחקה,',
			pageSizeChangeLabel: 'מספר שורות',
			gotoPageLabel: 'לך לעמוד',
			
        };
		var loading	 = 'טוען נתונים...';
		var numID    = 'מספר רשומה';
		var driverID = 'מספר נהג';
		var name     = 'שם';
		var family   = 'משפחה';
		var tz		 = 'ת"ז';
		var phone 	 = 'טלפון';
		var adrs	 = 'כתובת';
		var license	 = 'רישיון';
		var type_lice = 'סוג רישיון';
		var kablan   = 'קבלן';
		var email 	 = 'דוא"ל';
		var precent_down = 'אחוז הורדה';
		var type_rehev = 'סוג רכב';
		var num_rehev = 'מספר רכב';
		var nosim     = 'מס נוסעים';
		var polisa    = 'פוליסה';
		var deleteAll = 'מחק את השורות המסומנות';
		var customerID = 'מספר לקוח';
		var from  		= 'מוצא';
		var dest		= 'יעד';
		var nesiaID		= 'מספר חוזה';
		var price		= 'תשלום לנהג';
		var priceBefore = 'תשלום לנהג לפני מעמ';
		var priceAfter	= 'אחרי מעמ';
		var price_nesia = 'מחיר נסיעה';
		var desc		= 'תיאור';
		var time		= 'שעה';
		var totime		= 'שעת סיום';
		var date		= ' תאריך';
		var day1		= 'יום א';
		var day2		= 'יום ב';
		var day3		= 'יום ג';
		var day4		= 'יום ד';
		var day5		= 'יום ה';
		var day6		= 'יום ו';
		var day7		= 'יום ש';
		var search 		= 'חפש כאן...';
		var submit_search='שלח';
		var processed   = 'בוצע';
		var calcNesia		= 'חשב מחיר נסיעה';
		var calcDriver		= 'חשב תשלום לנהג';
		var resetf			= 'אפס';
		var priceHighMessage = 'התשלום לנהג יותר גבוה מהמחיר נסיעה';