﻿ //Localization texts
        var Messages = {
            serverCommunicationError: 'Ошибка связи с сервером.',
        loadingMessage: 'Загрузка...',
        noDataAvailable: 'Данные отсутствуют',
        addNewRecord: 'Добавить',
        editRecord: 'Изменить',
        areYouSure: 'Вы уверены?',
        deleteConfirmation: 'Удалить запись?',
        save: 'Сохранить',
        saving: 'Сохранение...',
        cancel: 'Отмена',
        deleteText: 'Удалить',
        deleting: 'Удаление...',
        error: 'Ошибка',
        close: 'Закрыть',
        cannotLoadOptionsFor: 'Невозможно загрузить варианты для поля {0}',
        pagingInfo: 'Записи с {0} по {1} из {2}',
        canNotDeletedRecords: 'Невозможно удалить записи: {0} из {1}!',
        deleteProggress: 'Удаление {0} из {1} записей...'
        };
		
		var numID    = 'Рекордное число';
		var driverID = 'Количество драйверов';
		var name     = 'название';
		var family   = 'семья';
		var tz		 = 'паспорт';
		var phone 	 = 'телефон';
		var adrs	 = 'адрес';
		var license	 = 'лицензия';
		var type_lice = 'Вид лицензии';
		var kablan   = 'подрядчик';
		var email 	 = 'E-mail';
		var precent_down = 'Процент Скачать';
		var type_rehev = 'автомобиль';
		var num_rehev = 'автомобиль ID';
		var nosim     = 'Количество пассажиров';
		var polisa    = 'политика';
		var deleteAll = 'Удалите строки отмечены';
		var customerID = 'номер клиента';
		var from  		= 'происхождение';
		var dest		= 'цель';
		var nesiaID		= 'номер контракта';
		var price		= 'цена';
		var price_nesia = 'плата за проезд';
		var desc		= 'описание';
		var time		= 'время';
		var date		= 'дата';
		var day1		= 'воскресенье';
		var day2		= 'понедельник';
		var day3		= 'вторник';
		var day4		= 'среда';
		var day5		= 'четверг';
		var day6		= 'пятница';
		var day7		= 'суббота';
		var search 		= 'Поиск ...';
		var submit_search='послать';
		var priceBefore = 'Оплата водителю без НДС';
		var priceAfter	= 'После НДС';
		var processed   = 'сделанный';