
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pl" xml:lang="pl">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content="ATTIA Technologies" />
<title>Tours - ATTIA Technologies</title>

<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<base target="_top"/>
<script type="text/javascript">
$(function(){
	$(".box .h_title").not(this).next("ul").hide("normal");
	$(".box .h_title").not(this).next("#home").show("normal");
	$(".box").children(".h_title").click( function() { $(this).next("ul").slideToggle(); });
});

</script>
<script type="text/javascript">
function navigate(url)
{
    var iframe = document.getElementsByTagName( "iframe" )[0];
    iframe.src = url;
}
</script>
<script>

$(document).keydown(function(e){
    if (e.keyCode == 113) { 
       window.open("helpDrivers/","myNewWin","width=330,height=500,toolbar=0");
		
    }
	if (e.keyCode == 115) { 
       window.open("helpCustomers/","myNewWin","width=330,height=500,toolbar=0"); 
    }
});
</script>
</head>
<?php 


session_start();
include 'loadlang.php';
include 'local/'.$lang.'.php';

echo '<link rel="stylesheet" type="text/css" href="css/style'.$langDir.'.css" media="screen" />
<link rel="stylesheet" type="text/css" href="css/navi'.$langDir.'.css" media="screen" />';
$db=$_SESSION['db'];
?>
<body>
<div class="wrap">
	<div id="header">
		<div id="top">
			<div class="right">
				<p> [ <a href="login/index.php">logout</a> ]</p>
			</div>
			<div class="left">
				
			</div>
		</div>
		<div id="nav">
			<ul>
				<li class="upp"><a href="#"><?php echo $mainControl;?></a>
					<ul>
						<li>&#8250; <a href="javascript:navigate('yoman/');" ><?php echo $yoman;?></a></li>
							
					</ul>
				</li>
				<li class="upp"><a href="javascript:navigate('nesiot/');" ><?php echo $nesiot;?></a></li>
				<li class="upp"><a href="javascript:navigate('customers/');"><?php echo $customers;?></a></li>
				<li class="upp"><a href="javascript:navigate('drivers/');"><?php echo $drivers;?></a></li>
				<li class="upp"><a href="javascript:navigate('settings/index.php?db=<?php echo $db;?>');"><?php echo $settings;?></a></li>
				
			</ul>
			<ul>
				<li class="upp"><a href="#"><?php echo $reports;?></a>
				<ul>
			<li><a href="javascript:navigate('reports/RepCust/index.php?db=<?php echo $db;?>');" ><?php echo $customer_details;?></a></li>
			<li><a href="javascript:navigate('reports/RepDriver/index.php?db=<?php echo $db;?>');" ><?php echo $driver_details;?></a></li>
			<li><a href="javascript:navigate('reports/RepNesiot/index.php?db=<?php echo $db;?>');" ><?php echo $nesiotReportHeader;?></a></li>
			<li><a href="javascript:navigate('reports/RepYoman/index.php?db=<?php echo $db;?>');" ><?php echo $print_yoman;?></a></li>
			<li><a href="javascript:navigate('reports/RepDriverYoman/index.php?db=<?php echo $db;?>');" ><?php echo $sum_driver;?></a></li>
			<li><a href="javascript:navigate('reports/RepCustYomanDate/index.php?db=<?php echo $db;?>');" ><?php echo $print_cust_date;?></a></li>
			
			<li><a href="javascript:navigate('reports/RepChart/index.php?db=<?php echo $db;?>');" ><?php echo $chartReportHeader;?></a></li>
	
				
				</ul>
				</li>
			</ul>
		</div>
	</div>
	
	<div id="content">
		
		<div id="main">
			<iframe src="yoman/" name="targetFrame" width="950px" height="520"></iframe>
			

			
		</div>
		<div class="clear"></div>
	</div>

	<div id="footer">
		<div class="left">
			
		</div>
		<div class="center">
			<p><a href="http://attiatech.net">ATTIA Technologies</a></p>
		</div>
	</div>
</div>

</body>
</html>
