<?php

$mainControl = 'תפריט ראשי';
$yoman 		 = 'יומן עבודה';
$customers 	 = 'קובץ לקוחות';
$drivers	 = 'קובץ נהגים';
$nesiot		 = 'קובץ חוזים';
$settings	 = 'הגדרות';
$reports	 = 'דוחות';
$customer_details='הדפסת פרטי לקוחות';
$driver_details='הדפסת פרטי נהג קבלן';
$print_yoman='הדפסת יומן עבודה יומי ';
$sum_driver='הדפסת סך הנסיעות והתשלום לפי נהג\קבלן';
$sum_customer='הדפסת סך הנסיעות והתשלום לפי לקוח';
$small_by_cust='הדפסת יומן נסיעות מצומצם לפי לקוח';
$small_by_driver='הדפסת יומן נסיעות מצומצם לפי נהג';
$print_cust_date='הדפסת סך הנסיעות לפי לקוח בין תאריכים';
$print_cust_date_min= 'הדפסת סך הנסיעות לפי לקוח בין תאריכים מצומצם';
$countHeader	='כמות';
$oneTimeNesiaHeader	 ='נסיעה חד פעמית';
// reports section

$customerReportHeader = 'דוח פרטי לקוח';
$customerNumberHeader ='מספר לקוח';
$nameHeader 		  = 'שם';
$addressHeader		  = 'כתובת';
$phoneHeader		  = 'טלפון';
$driverReportHeader	  = 'דוח פרטי נהג\קבלן';
$driverNumberHeader	  = 'מספר נהג';
$tzHeader			  = 'תעודת זהות';
$noResultHeader		  = 'אין תוצאות';
$licenseHeader		  = 'רישיון';
$nesiotReportHeader   = 'הדפסת חוזים\מכרז';
$nesiaNumberHeader	  = 'מספר חוזה';
$fromHeader			  = 'מוצא';
$destHeader		      = 'יעד';
$dateHeader			  = 'תאריך';
$descHeader			  = 'תיאור';
$timeHeader			  = 'שעה';
$totimeHeader			  = 'שעת סיום';
$yomanReportHeader = 'הדפסת יומן עבודה יומי';
$yomanDriverReportHeader = 'סך הנסיעות לפי נהג';
$totalHeader			='סך הכל';
$yomanCustReportHeader = 'סך הנסיעות לפי לקוח';
$priceHeader			= 'תשלום';
$priceDriver			= 'תשלום לנהג';
$priceDriverBoded			= 'תשלום לנהג בודד';
$priceNesia				= 'מחיר נסיעה';
$priceNesiaBoded		= 'מחיר נסיעה בודדת';
$totalBefore			= 'מחיר לפני מעמ';
$totalAfter  			= 'מחיר אחרי מעמ';
$payHeader				= 'תשלום';
$drivesHeader			= 'נסיעות';
$maamHeader				= 'מעמ';
$start					= 'התחלה';
$end					= 'סיום';
$CustDateyomanReportHeader='סך הנסיעות לפי לקוח בין תאריכים';
$procHeader				= 'בוצע';
$yes					= 'כן';
$no						= 'לא';
$showPrices				= 'הצגת מחירים ביומן';
$day1		= 'יום א';
		$day2		= 'יום ב';
		$day3		= 'יום ג';
		$day4		= 'יום ד';
		$day5		= 'יום ה';
		$day6		= 'יום ו';
		$day7		= 'יום ש';
$settingsHeader     = 'הגדרות מערכת';
$msg 				= ' חתך לפי ';
$general 			= 'כללי';
$execDate			= 'תאריך הוצאת דוח';
$reportDetails		= 'פרטי דוח:';
$backUpHeader		= 'גבה את המידע';
$helpCustHeader     = 'טבלת עזר לקוחות';
$helpDriveHeader     = 'טבלת עזר נהגים';



$chartReportHeader	= 'גרף הוצאות מול הכנסות';
$sumNesia 			= 'תשלומי נסיעות - הכנסה';
$totalErn		 	= 'רווח';
$sumDriver			= 'תשלומים לנהגים - הוצאה';


$filterByCust       = 'סנן לפי לקוח';
$filterByDriver      = 'סנן לפי נהג';
?>