<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pl" xml:lang="pl">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<title>טורס - מערכת לניהול הסעים</title>
<link rel="stylesheet" type="text/css" href="css/login.css" media="screen" />
<script type="text/javascript" src="../jquery.js"></script>

</head>
<body>

<div class="wrap">
	<div id="content">
	<img src="../logo4.png" style="top:8px;right:0px;position:absolute;"/>
		<div id="main">
		
			<div class="full_w">
			<div class="errorMessage" style="margin-left:85px;position:relative;">
			<img src="img/i_warning.png" id="warningIcon" style="display:none;"/>
			<img src="img/i_ok.png" id="okIcon" style="display:none;"/>
			<input    type="text" id="msg" readonly="yes" style="color:red;border:none;width:200px;padding:4px;"/>
			</div>	
				<div id="form"  >
					<label for="login">Username:</label>
					<input id="login" name="username" class="text" />
					<label for="pass">Password:</label>
					<input id="pass" name="password" type="password" class="text" />
					<div class="sep"></div>
					<img src="loading.gif" style="display: none;" id="loading_image">
					<button type="submit" id="submit" class="ok">Login</button> <button  id="recoverPass">Forgot your password?</button>
				</div>
			</div>
			<div class="footer">&raquo; <a href="http://attiatech.net/tours.html">http://attiatech.net</a> <b>מערכת לניהול הסעים</b></div>
		</div>
	</div>
</div>
<script>
$( "#submit" ).click(function(){
var username = $('#login').val();
var password = $('#pass').val();
if(username=='' && password==''){

						$('#msg').val('Insert username and password ');
						$('.errorMessage').fadeIn();
						$('#warningIcon').fadeIn();
						setTimeout(function() {
							$('#warningIcon').fadeOut();
							$('.errorMessage').fadeOut();
						}, 3000);
							return
}
$('#loading_image').show();
$.ajax({
					url: 'check.php',
					type: 'GET',
					data: {
					username: username,
					password: password
					},	
					
					success:function (data) {
					$('#loading_image').hide();
					if(data=='1'){
						
						var url='../index.php';
						$(location).attr('href',url);
						
					}
					else{
					
						
						$('#login').val(username);
						$('#msg').val('Check your username of password please');
						$('.errorMessage').fadeIn();
						$('#warningIcon').fadeIn();
						setTimeout(function() {
							$('#warningIcon').fadeOut();
							$('.errorMessage').fadeOut();
						}, 3000);
					}
				 }
			}); 
			});
</script>
<script>
$( "#recoverPass" ).click(function(){
var mail = $('#login').val();
if(mail==''){
$('#login').val(mail);
						$('#msg').val('Insert your mail for recover');
						$('.errorMessage').fadeIn();
						$('#warningIcon').fadeIn();
						setTimeout(function() {
							$('#warningIcon').fadeOut();
							$('.errorMessage').fadeOut();
						}, 3000);
							return
}

$('#loading_image').show();
$.ajax({
					url: 'recover.php',
					type: 'GET',
					data: {
					mail: mail
					
					},	
					
					success:function (data) {
					
					$('#loading_image').hide();
					if(data==1){
						
						
						$('#msg').val('Your password sent to you email');
						$('.errorMessage').fadeIn();
						$('#okIcon').fadeIn();
						setTimeout(function() {
							$('#okIcon').fadeOut();
							$('.errorMessage').fadeOut();
						}, 3000);
						
					}
					else{
					
						
						$('#login').val(mail);
						$('#msg').val('You are not registered');
						$('.errorMessage').fadeIn();
						$('#warningIcon').fadeIn();
						setTimeout(function() {
							$('#warningIcon').fadeOut();
							$('.errorMessage').fadeOut();
						}, 3000);
					}
				 }
			}); 
			});
</script>

</body>
</html>
