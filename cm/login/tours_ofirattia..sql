-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 30, 2016 at 02:47 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `tours_ofirattia.`
--
CREATE DATABASE IF NOT EXISTS `tours_ofirattia.` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `tours_ofirattia.`;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
`id` int(20) NOT NULL,
  `customer_id` int(20) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `phone` varchar(20) CHARACTER SET utf8 NOT NULL,
  `adrs` varchar(100) CHARACTER SET utf8 NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `precent_down` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=hebrew AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `drivers`
--

CREATE TABLE IF NOT EXISTS `drivers` (
`id` int(11) NOT NULL,
  `driver_id` int(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `family` varchar(100) NOT NULL,
  `tz` varchar(15) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `adrs` varchar(100) NOT NULL,
  `license` int(15) NOT NULL,
  `type_lice` varchar(9) NOT NULL,
  `email` varchar(50) NOT NULL,
  `kablan` varchar(15) NOT NULL,
  `pic` varchar(60) NOT NULL,
  `precent_down` int(20) NOT NULL,
  `type_rehev` varchar(20) NOT NULL,
  `num_rehev` int(20) NOT NULL,
  `nosim` int(20) NOT NULL,
  `polisa` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `nesiot`
--

CREATE TABLE IF NOT EXISTS `nesiot` (
`id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `desc` varchar(100) NOT NULL,
  `from` varchar(100) NOT NULL,
  `dest` varchar(100) NOT NULL,
  `driver` int(20) NOT NULL,
  `days` varchar(8) NOT NULL,
  `customer` varchar(20) NOT NULL,
  `price` float NOT NULL,
  `price_nesia` float NOT NULL,
  `nesia_id` int(30) NOT NULL,
  `day1` int(1) NOT NULL,
  `day2` int(1) NOT NULL,
  `day3` int(1) NOT NULL,
  `day4` int(1) NOT NULL,
  `day5` int(1) NOT NULL,
  `day6` int(1) NOT NULL,
  `day7` int(1) NOT NULL,
  `open` date NOT NULL,
  `totime` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `open_close`
--

CREATE TABLE IF NOT EXISTS `open_close` (
`id` int(10) NOT NULL,
  `open_yoman` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE IF NOT EXISTS `setting` (
`id` int(11) NOT NULL,
  `notification` int(11) NOT NULL,
  `precent` varchar(6) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `show_price` int(2) NOT NULL,
  `date` date NOT NULL,
  `lang_dir` int(1) NOT NULL,
  `lang` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `lastDate` date NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `notification`, `precent`, `show_price`, `date`, `lang_dir`, `lang`, `lastDate`) VALUES
(1, 1, '', 0, '0000-00-00', 0, '0', '0000-00-00'),
(2, 0, '17', 0, '0000-00-00', 0, '0', '0000-00-00'),
(3, 0, '', 1, '0000-00-00', 0, '0', '0000-00-00'),
(4, 0, '', 0, '0000-00-00', 0, 'he', '0000-00-00'),
(5, 0, '', 0, '0000-00-00', 0, '', '2016-09-29');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(10) NOT NULL,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name2` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `hash` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `fullname` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  `date_start` int(10) NOT NULL,
  `Image` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pass` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `customer_id` (`customer_id`);

--
-- Indexes for table `drivers`
--
ALTER TABLE `drivers`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `driver_id` (`driver_id`);

--
-- Indexes for table `nesiot`
--
ALTER TABLE `nesiot`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `nesia_id` (`nesia_id`);

--
-- Indexes for table `open_close`
--
ALTER TABLE `open_close`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `drivers`
--
ALTER TABLE `drivers`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `nesiot`
--
ALTER TABLE `nesiot`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `open_close`
--
ALTER TABLE `open_close`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;