-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 10, 2013 at 05:45 PM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `attiatec_tours_misha`
--

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE IF NOT EXISTS `setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notification` int(11) NOT NULL,
  `precent` varchar(6) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `show_price` int(2) NOT NULL,
  `date` date NOT NULL,
  `lang_dir` int(1) NOT NULL,
  `lang` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `lastDate` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `notification`, `precent`, `show_price`, `date`, `lang_dir`, `lang`, `lastDate`) VALUES
(1, 1, '', 0, '0000-00-00', 0, '0', '0000-00-00'),
(2, 0, '18', 0, '0000-00-00', 0, '0', '0000-00-00'),
(3, 0, '', 1, '0000-00-00', 0, '0', '0000-00-00'),
(4, 0, '', 0, '0000-00-00', 0, 'he', '0000-00-00'),
(5, 0, '', 0, '0000-00-00', 0, '', '2013-11-11');
