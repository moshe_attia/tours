-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 10, 2013 at 05:44 PM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `attiatec_tours_misha`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `customer_id` int(20) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `phone` varchar(20) CHARACTER SET utf8 NOT NULL,
  `adrs` varchar(100) CHARACTER SET utf8 NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `precent_down` int(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `customer_id` (`customer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=hebrew AUTO_INCREMENT=17 ;

-- --------------------------------------------------------

--
-- Table structure for table `drivers`
--

CREATE TABLE IF NOT EXISTS `drivers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `driver_id` int(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `family` varchar(100) NOT NULL,
  `tz` varchar(15) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `adrs` varchar(100) NOT NULL,
  `license` int(15) NOT NULL,
  `type_lice` varchar(9) NOT NULL,
  `email` varchar(50) NOT NULL,
  `kablan` varchar(15) NOT NULL,
  `pic` varchar(60) NOT NULL,
  `precent_down` int(20) NOT NULL,
  `type_rehev` varchar(20) NOT NULL,
  `num_rehev` int(20) NOT NULL,
  `nosim` int(20) NOT NULL,
  `polisa` int(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `driver_id` (`driver_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

-- --------------------------------------------------------

--
-- Table structure for table `nesiot`
--

CREATE TABLE IF NOT EXISTS `nesiot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `desc` varchar(100) NOT NULL,
  `from` varchar(100) NOT NULL,
  `dest` varchar(100) NOT NULL,
  `driver` int(20) NOT NULL,
  `days` varchar(8) NOT NULL,
  `customer` varchar(20) NOT NULL,
  `price` float NOT NULL,
  `price_nesia` float NOT NULL,
  `nesia_id` int(30) NOT NULL,
  `day1` int(1) NOT NULL,
  `day2` int(1) NOT NULL,
  `day3` int(1) NOT NULL,
  `day4` int(1) NOT NULL,
  `day5` int(1) NOT NULL,
  `day6` int(1) NOT NULL,
  `day7` int(1) NOT NULL,
  `open` date NOT NULL,
  `totime` time NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nesia_id` (`nesia_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=152 ;

-- --------------------------------------------------------

--
-- Table structure for table `open_close`
--

CREATE TABLE IF NOT EXISTS `open_close` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `open_yoman` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE IF NOT EXISTS `setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notification` int(11) NOT NULL,
  `precent` varchar(6) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `show_price` int(2) NOT NULL,
  `date` date NOT NULL,
  `lang_dir` int(1) NOT NULL,
  `lang` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `lastDate` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name2` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `hash` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `fullname` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  `date_start` int(10) NOT NULL,
  `Image` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pass` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `yoman2013`
--

CREATE TABLE IF NOT EXISTS `yoman2013` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `customer` int(20) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `desc` varchar(100) NOT NULL,
  `from` varchar(100) NOT NULL,
  `dest` varchar(100) NOT NULL,
  `driver` int(20) NOT NULL,
  `price` varchar(20) NOT NULL,
  `days` varchar(20) NOT NULL,
  `processed` int(1) NOT NULL,
  `price_nesia` int(20) NOT NULL,
  `nesia_id` int(30) NOT NULL,
  `totime` time NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `driver` (`driver`),
  KEY `date` (`date`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3642 ;

-- --------------------------------------------------------

--
-- Table structure for table `yoman2014`
--

CREATE TABLE IF NOT EXISTS `yoman2014` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `customer` int(20) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `desc` varchar(100) NOT NULL,
  `from` varchar(100) NOT NULL,
  `dest` varchar(100) NOT NULL,
  `driver` int(20) NOT NULL,
  `price` varchar(20) NOT NULL,
  `days` varchar(20) NOT NULL,
  `processed` int(1) NOT NULL,
  `price_nesia` int(20) NOT NULL,
  `nesia_id` varchar(30) NOT NULL,
  `totime` time NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `driver` (`driver`),
  KEY `date` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
