<?php 

session_start();
if(isset($_REQUEST['db']))
	{
		$db=$_REQUEST['db'];
		$_SESSION['db']=$db;
	}
$db=$_SESSION['db'];
include '../data/drivers.php';
include '../data/customers.php';
include '../loadlang.php';
include '../data/loadPrecent.php';
?>
<html>
  <head>

    <link href="../files/themes/redmond/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
	<meta http-equiv='Content-Type' content='Type=text/html; charset=utf-8'>

	<script src="../files/scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="../files/scripts/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
    <script src="../files/scripts/jtable/jquery.jtable.js" type="text/javascript"></script>
	<?php
	echo '<script src="../files/scripts/jtable/localization/'.$lang.'.js" type="text/javascript"></script>
    <link href="../files/scripts/jtable/themes/metro/blue/jtable'.$langDir.'.css" rel="stylesheet" type="text/css" />';
	?>
	    <script src="../files/lib/mask.js" type="text/javascript"></script>

 <script>
 
 // this is for price driver
function calc3() { 
  //var one = Number( document.getElementById('price').value); 
  //var two = Number( document.getElementById('priceAfter').value);
var one =Number($('#price').val());
  var two = Number($('#priceAfter2').val());
 
 if(one==0){
	var res = two-((two*<?php echo $n;?>)/<?php echo $n2;?>);
	document.getElementById('price').value=res.toFixed(2);
    
 }
 else if(two==0){
 
  var res = parseFloat(parseFloat($('#price').val())+parseFloat($('#price').val()*<?php echo $n;?>/100));
  console.log(res);
	document.getElementById('priceAfter2').value=res.toFixed(2);
    
 }
 else if(one!=0 && two!=0){
		alert("Error : Delete 1 field");
 
 }
	return;
  
  }
 
 
 // this is for price nesia
 function calc4() { 
  //var one = Number( document.getElementById('price').value); 
  //var two = Number( document.getElementById('priceAfter').value);
var one =Number($('#price_nesia').val());
  var two = Number($('#price_nesiaAfter2').val());

 if(one==0){
	var res = two-((two*18)/118);
	document.getElementById('price_nesia').value=res.toFixed(2);
    
 }
 else if(two==0){
 
  var res = parseFloat(parseFloat($('#price_nesia').val())+parseFloat($('#price_nesia').val()*18/100));
  console.log(res);
	document.getElementById('price_nesiaAfter2').value=res.toFixed(2);
    
 }
 else if(one!=0 && two!=0){
		alert("Error : Delete 1 field");
 
 }
	return;
  
  }
 </script>
 <SCRIPT TYPE="text/JavaScript">
    function validateHhMm(inputField) {
        var isValid = /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(inputField.value);

        if (isValid) {
            inputField.style.backgroundColor = '#bfa';
        } else {
            inputField.style.backgroundColor = '#fba';
			alert("Please check your time again..");
        }

        return isValid;
    }
</SCRIPT>
  </head>
<body style="background: url(../files/images/bgnoise_lg.png) repeat left top;">
  <div class="filtering">
    <form>
        <input type="text" name="search" id="search" placeholder="Search" />
		<input type="submit" id="LoadRecordsButton" value="Go">
        <input type="date" name="datesearch" id="datesearch" />
        
    </form>
</div>
	<div id="PeopleTableContainer" style="width: 900px;"></div>
	 <input type="button" id="DeleteAllButton"  value="Delete All Selected Rows"/>
	 <script type="text/javascript">
	//initial input form
	 $('#DeleteAllButton').val(deleteAll);
	 $('#LoadRecordsButton').val(submit_search);
	 $('#search').attr("placeholder", search);
	
</script>
	
	<script type="text/javascript">

		$(document).ready(function () {

		    //Prepare jTable
			$('#PeopleTableContainer').jtable({
				title: 'טבלת חוזים',
				 messages: Messages, //Lozalize
			  paging: true, //Enable paging
            pageSize: 5, //Set page size (default: 10)
            sorting: true, //Enable sorting
            defaultSorting: 'nesia_id ASC', //Set default sorting
            selecting: true, //Enable selecting
            multiselect: true, //Allow multiple selecting
            selectingCheckboxes: true, //Show checkboxes on first column
			columnResizable: true, //Disable column resizing
            columnSelectable: true, //Disable column selecting
            saveUserPreferences: true, //Actually, no need to set true since it's default
          
            openChildAsAccordion: true,

            //selectOnRowClick: false, //Enable this to only select using checkboxes

				actions: {
					listAction: 'listActions.php?action=list&db=<?php echo $db; ?>',
					createAction: 'listActions.php?action=create&db=<?php echo $db; ?>',
					updateAction: 'listActions.php?action=update&db=<?php echo $db; ?>',
					deleteAction: 'listActions.php?action=delete&db=<?php echo $db; ?>'
				},
				fields: {
					id: {
						title: numID,
						key: true,
						list: false,
						
						
					},
					nesia_id: {
						title: nesiaID,
						width: '8%',
						edit: true,
						
						create: true,
						input: function (data) {
        if (data.record) {
            return '<input type="text" readonly="yes" name="nesia_id" style="width:40px" value="' + data.record.nesia_id + '" />';
        } else {
		
		 
		 return '<input  type="text" name="nesia_id"  style="width:40px"  />';

        }
    }
					},
					
					customer: {
						title: customerID,
						width: '16%',
						options:<?php echo $customerOptions; ?>,

						create: true,
						edit: true,
		
					},
			
					from: {
						title: from,
						width: '12%',
						
						create: true,
						edit: true
					},
					
					dest: {
						title: dest,
						width: '11%',
						
						create: true,
						edit: true
					},
					
					date: {
						title: date,
						width: '13%',
						edit: true,
						create: true,
						 input: function (data) {
        if (data.record) {
            return '<input type="date" name="date" style="width:150px" value="' + data.record.date + '" />';
        } else {
            return '<input  type="date" name="date"  style="width:150px"  />';
        }
    }
					},
					
					
					time: {
						title: time,
						width: '20%',
						
						create: true,
						edit: true,
						input: function (data) {
        if (data.record) {
            return '<input type="text" name="time" onchange="validateHhMm(this);" id="time" style="width:150px" value="' + data.record.time + '" />';
        } else {
			
		
		
            return '<input  type="text" name="time"  onchange="validateHhMm(this);" id="time" style="width:150px"  />';
        }
    }
					},
					totime: {
						title: totime,
						width: '9%',
						sorting:false,
						create: true,
						edit: true,
						input: function (data) {
						if (data.record) {
							return '<input type="text" onchange="validateHhMm(this);"  id="totime" name="totime" style="width:150px" value="' + data.record.totime + '" />';
						} else {
							return '<input  type="text" onchange="validateHhMm(this);" id="totime"  name="totime"  style="width:150px"  />';
						}
					}
					},
					desc: {
						title: desc,
						width: '40%',
						list:false,
						create: true,
						
						edit: true
					},
					
					
					
					driver: {
						title: driverID,
						width: '40%',
						
						create: true,
						edit: true,
						 options:<?php echo $driversOptions; ?>,
	
					},


					price: {
						title: priceBefore,
						width: '40%',
						list:false,
						create: true,
						edit: true,
								input: function (data) {
        if (data.record) {
            return '<input type="text" id="price" name="price"  style="width:90px" value="' + data.record.price + '" /><br><br><input type="text" style="width:90px" name="priceAfter" placeholder="'+priceAfter+'" id="priceAfter2"  /><input type="button" value="'+calcDriver+'" onclick="calc3()" />';
        } else {
            return '<input  type="text" id="price" name="price"   style="width:90px"  /><br><br><input type="text" name="priceAfter2" style="width:90px" placeholder="'+priceAfter+'" id="priceAfter2"  /><br><br><input type="button" value="'+calcDriver+'" onclick="calc3()" />';
        }
    }
					},
					
					price_nesia: {
						title: price_nesia,
						width: '40%',
						list:false,
						create: true,
						edit: true,
						input: function (data) {
        if (data.record) {
            return '<input type="text" id="price_nesia" name="price_nesia"  style="width:90px" value="' + data.record.price_nesia + '" /><br><br><input type="text" style="width:90px" name="price_nesiaAfter2" placeholder="'+priceAfter+'" id="price_nesiaAfter2"  /><input type="button" value="'+calcNesia+'" onclick="calc4()" />';
        } else {
            return '<input  type="text" id="price_nesia" name="price_nesia"   style="width:90px"  /><br><br><input type="text" name="price_nesiaAfter2" style="width:90px" placeholder="'+priceAfter+'" id="price_nesiaAfter2"  /><br><br><input type="button" value="'+calcNesia+'" onclick="calc4()" />';
        }
    }
					},
												emptycol4:{
					title:'',
					list:false,
					edit:true,
					create:false,
					input:function(data){
					return '<input type="text" style="border:none;"  readonly="yes"/>';
					}



},
					day1: {
						title: day1,
						width: '30%',
						
						list:false,
						create: true,
						edit: true,
						options: { '1': 'כן',
								   '0': 'לא'
								   
									
						},
					},
					day2: {
						title: day2,
						width: '30%',
						list:false,
						create: true,
						edit: true,
						options: { '1': 'כן',
								   '0': 'לא'
								   
									
						},
					},
		
					day3: {
						title: day3,
						width: '30%',
						list:false,
						create: true,
						edit: true,
						options: { '1': 'כן',
								   '0': 'לא'
								   
									
						},
					},
	
					day4: {
						title: day4,
						width: '30%',
						list:false,
						create: true,
						edit: true,
						options: { '1': 'כן',
								   '0': 'לא'
								   
									
						},
					},
					day5: {
						title: day5,
						width: '30%',
						list:false,
						create: true,
						edit: true,
						options: { '1': 'כן',
								   '0': 'לא'
								   
									
						},
					},
					day6: {
						title: day6,
						width: '30%',
						list:false,
						create: true,
						edit: true,
						options: { '1': 'כן',
								   '0': 'לא'
								   
									
						},
					},
					day7: {
						title: day7,
						width: '30%',
						list:false,
						create: true,
						edit: true,
						options: { '1': 'כן',
								   '0': 'לא'
								   
									
						},
				
					}
				
					
					
					
					
				},
				formCreated: function (event, data) {
        data.form.find('[name=time]').mask('99:99');
		data.form.find('[name=totime]').mask('99:99');
    },
	//Validate form when it is being submitted
            formSubmitting: function (event, data) {
                if(data.form.find('[name=nesia_id]').val()==''){alert("ERROR : "+nesiaID);return false;}
				if(data.form.find('[name=from]').val()==''){alert("ERROR : "+from);return false;}
				if(data.form.find('[name=dest]').val()==''){alert("ERROR : "+dest);return false;}
				if(data.form.find('[name=date]').val()==''){alert("ERROR : "+date);return false;}
				if(validateSubmit($('#time').val())==false ){alert("ERROR : "+time);return false;}
				if(validateSubmit($('#totime').val())==false){alert("ERROR : "+totime);return false;}
				if(data.form.find('[name=desc]').val()==''){alert("ERROR : "+desc);return false;}
				else{
					
					return true;
				
				}
				
            },
			rowInserted: function (event, data) {
					if(data.record){
							var rowCount = $('.jtable tr').length;
					    var d=data.record.date.split("-");
						var newd=d[2]+'-'+d[1]+'-'+d[0];// build new date
						$(".jtable tr:eq("+(rowCount-1)+") td:eq(5)").text(newd);
						}
						}
			});
			
			//Load person list from server
			$('#PeopleTableContainer').jtable('load');
			 $('#DeleteAllButton').button().click(function () {
            var $selectedRows = $('#PeopleTableContainer').jtable('selectedRows');
            $('#PeopleTableContainer').jtable('deleteRows', $selectedRows);
        });
		//Re-load records when user click 'load records' button.
        $('#LoadRecordsButton').click(function (e) {
            e.preventDefault();
            $('#PeopleTableContainer').jtable('load', {
                search: $('#search').val(),
                
            });
        });
		$( "#datesearch" ).blur(function(e) {
		//alert($('#datesearch').val());
		 e.preventDefault();
 $('#PeopleTableContainer').jtable('load', {
                datesearch: $('#datesearch').val(),
                
            });
});
        
		$('#PeopleTableContainer').jtable('option', 'pageSize',10);
		
		// this is for the content of the table
			var fontSize = parseInt($('.jtable').css("font-size"));
			
			fontSize = "12";
			$('.jtable').css({'font-size':fontSize});
			// this is for the headers of the table
			var fontSizetHead= parseInt($('th').css("font-size"));
		
			fontSizetHead = "12";
			$('th').css({'font-size':fontSizetHead});
		
		
		
		
		
		
		
		});

	</script>

  </body>
</html>
