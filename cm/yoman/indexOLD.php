<?php 

session_start();
if(isset($_REQUEST['db']))
	{
		$db=$_REQUEST['db'];
		$_SESSION['db']=$db;
	}
$db=$_SESSION['db'];
include '../data/getDate.php';
include '../data/drivers.php';
include '../data/customers.php';
include_once '../openNesiot.php';
include '../loadlang.php';
include '../local/'.$lang.'.php';
include '../data/loadPrecent.php';
include '../data/showPrices.php';
$today=date('Y-m-d');
?>
<html>
  <head>

    <link href="../files/themes/redmond/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
	<meta http-equiv='Content-Type' content='Type=text/html; charset=utf-8'>

	<script src="../files/scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="../files/scripts/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
    <script src="../files/scripts/jtable/jquery.jtable.js" type="text/javascript"></script>
	    <script src="../files/scripts/date.js" type="text/javascript"></script>

	    <script src="../files/lib/mask.js" type="text/javascript"></script>

	
	
	<?php
	echo '<script src="../files/scripts/jtable/localization/'.$lang.'.js" type="text/javascript"></script>
    <link href="../files/scripts/jtable/themes/metro/crimson/jtable'.$langDir.'.css" rel="stylesheet" type="text/css" />';
	?>
	<script>

 </script>
 <script>
 
 // this is for price driver
function calc3() { 
  //var one = Number( document.getElementById('price').value); 
  //var two = Number( document.getElementById('priceAfter').value);
var one =Number($('#price').val());
var two = Number($('#priceAfter2').val());

 if(one==0){
	var res = two-((two*<?php echo $n;?>)/<?php echo $n2;?>);
	document.getElementById('price').value=res.toFixed(2);
    
 }
 else if(two==0){
 
  var res = parseFloat(parseFloat($('#price').val())+parseFloat($('#price').val()*<?php echo $n;?>/100));
  console.log(res);
	document.getElementById('priceAfter2').value=res.toFixed(2);
    
 }
 else if(one!=0 && two!=0){
		alert("Error : Delete 1 field");
 
 }
	return;
  
  }
 
 
 // this is for price nesia
 function calc4() { 
  //var one = Number( document.getElementById('price').value); 
  //var two = Number( document.getElementById('priceAfter').value);
var one =Number($('#price_nesia').val());
  var two = Number($('#price_nesiaAfter2').val());
 
 if(one==0){
	var res = two-((two*<?php echo $n;?>)/<?php echo $n2;?>);
	document.getElementById('price_nesia').value=res.toFixed(2);
    
 }
 else if(two==0){
 
  var res = parseFloat(parseFloat($('#price_nesia').val())+parseFloat($('#price_nesia').val()*<?php echo $n;?>/100));
  console.log(res);
	document.getElementById('price_nesiaAfter2').value=res.toFixed(2);
    
 }
 else if(one!=0 && two!=0){
		alert("Error : Delete 1 field");
 
 }
	return;
  
  }
 </script>

  </head>
<body style="background: url(../files/images/bgnoise_lg.png) repeat left top;">
	<div id="results"></div>

<div style="position:absolute;left:5px;border:1px solid;">F4 - <?php echo $helpCustHeader;?>, F2 - <?php echo $helpDriveHeader;?></div>
  <div class="filtering">
    <form>
        <input type="text" name="search" id="search" placeholder="Search" />
		<input type="submit" id="LoadRecordsButton" value="Go">
       <input type="date" name="datesearch" id="datesearch" value="<?php if($lastDate!='')echo $lastDate;?>"/>
        <?php echo $filterByCust; ?>
		<select id="filterByCust">
		<option></option>
		</select>
		<input type="button" id="filterBy" value="Go">
    </form>
</div>
	<div id="PeopleTableContainer" style="width: 900px;"></div>
	 <input type="button" id="DeleteAllButton"  value="Delete All Selected Rows"/>
	 <script type="text/javascript">
	//initial input form
	 $('#DeleteAllButton').val(deleteAll);
	 $('#LoadRecordsButton').val(submit_search);
	 $('#filterBy').val(submit_search);
	 $('#search').attr("placeholder", search);
	  var json=<?php echo $customerOptions; ?>;
	 $.each( json, function( key, value ) {
		$('#filterByCust')
         .append($("<option></option>")
         .attr("value",key)
         .text(value));
	});
	 $('#filterBy').click(function () {
	
     var optionSelected = $('#filterByCust').find("option:selected");
     var valueSelected  = optionSelected.val();
     var textSelected   = optionSelected.text();
	 console.log(valueSelected);
	 if(valueSelected!=''){
	 $('#PeopleTableContainer').jtable('load', {
                filterByCust: valueSelected,
                
            });
			}
	else{
	
	 $('#PeopleTableContainer').jtable('load', {
               search:'',
                
            });
	
	
	}
 });

	
</script>

	<script type="text/javascript">
var index=0;
		$(document).ready(function () {
		
		    //Prepare jTable
			$('#PeopleTableContainer').jtable({
			title: 'יומן נסיעות',
			messages: Messages, //Lozalize
			paging: true, //Enable paging
            pageSize: 7, //Set page size (default: 10)
            sorting: true, //Enable sorting
            defaultSorting: 'date ASC time ASC', //Set default sorting  //order by date DESC, time DESC
            selecting: true, //Enable selecting
            multiselect: true, //Allow multiple selecting
            selectingCheckboxes: true, //Show checkboxes on first column
			
            columnResizable: true, //Disable column resizing
            columnSelectable: true, //Disable column selecting
            saveUserPreferences: true, //Actually, no need to set true since it's default
            openChildAsAccordion: true,

            //selectOnRowClick: false, //Enable this to only select using checkboxes

				actions: {
					listAction: 'listActions.php?action=list&db=<?php echo $db; ?>',
					createAction: 'listActions.php?action=create&db=<?php echo $db; ?>',
					updateAction: 'listActions.php?action=update&db=<?php echo $db; ?>',
					deleteAction: 'listActions.php?action=delete&db=<?php echo $db; ?>'
				},
				fields: {
					id: {
						title: numID,
						key: true,
						list: false,
						
						
					},
					nesia_id: {
						title: nesiaID,
						width: '5%',
						edit: false,
						list:true,
						sorting:true,
						create: false
					},
					
					customer: {
						title: customerID,
						width: '9%',
						options:<?php echo $customerOptions; ?>,
						sorting:true,
						create: true,
						edit: true,
		
					},
					desc: {
						title: desc,
						width: '6%',
						list:true,
						create: true,
						sorting:true,
						edit: true
					},
					from: {
						title: from,
						width: '12%',
						sorting:true,
						create: true,
						edit: true
					},
					
					dest: {
						title: dest,
						width: '12%',
						sorting:true,
						create: true,
						edit: true
					},
					date: {
						title: date,
						width: '13%',
						edit: true,
						create: true,
						
						 input: function (data) {
        if (data.record) {
		
            return '<input type="date" name="date" style="width:150px" value="' + data.record.date + '" />';
        } else {
            return '<input  type="date" name="date"  style="width:150px"  />';
        }
    }
					},
					emptycol3:{
					title:'',
					list:false,
					edit:true,
					input:function(data){
					return '<input type="text" style="border:none;"  readonly="yes"/>';
					}



},
					
					
					time: {
						title: time,
						width: '9%',
						
						create: true,
						edit: true,
						input: function (data) {
						if (data.record) {
							return '<input type="text" name="time" onchange="validateHhMm(this);"  id="time" style="width:150px" value="' + data.record.time + '" />';
						} else {
							return '<input  type="text" name="time"  onchange="validateHhMm(this);" id="time" style="width:150px"  />';
						}
					}
					},
					totime: {
						title: totime,
						width: '9%',
						sorting:false,
						create: true,
						edit: true,
						input: function (data) {
						if (data.record) {
							return '<input type="text" onchange="validateHhMm(this);" name="totime" id="totime" style="width:150px" value="' + data.record.totime + '" />';
						} else {
							return '<input  type="text" onchange="validateHhMm(this);" name="totime"  id="totime" style="width:150px"  />';
						}
					}
					},
					
					
					
					driver: {
						title: driverID,
						width: '6%',
						sorting:false,
						create: true,
						edit: true,
						 options:<?php echo $driversOptions; ?>,
	
					},
			maam:{
					title:'<?php echo $maamHeader;?>',
					list:false,
					sorting:false,
					input:function(data){
					return '<input type="text" style="border:none;" value="<?php echo $maamHeader.' :'.$n.'%';?>" readonly="yes"/>';
					}



},	
		emptycol:{
					title:'',
					list:false,
					edit:true,
					input:function(data){
					return '<input type="text" style="border:none;"  readonly="yes"/>';
					}



},
	emptycol2:{
					title:'',
					list:false,
					edit:true,
					input:function(data){
					return '<input type="text" style="border:none;"  readonly="yes"/>';
					}



},		

					price: {
						title: price,
						width: '7%',
						list:<?php echo $showPrice;?>,
						create: true,
						sorting:false,
						edit: true,
								input: function (data) {
        if (data.record) {
            return '<input type="text" id="price" name="price"  style="width:90px" value="' + data.record.price + '" /><br><br><input type="text" style="width:90px" name="priceAfter" placeholder="'+priceAfter+'" id="priceAfter2"  /><input type="button" value="'+calcDriver+'" onclick="calc3()" />';
        } else {
            return '<input  type="text" id="price" name="price"   style="width:90px"  /><br><br><input type="text" name="priceAfter2" style="width:90px" placeholder="'+priceAfter+'" id="priceAfter2"  /><br><br><input type="button" value="'+calcDriver+'" onclick="calc3()" />';
        }
    }
					},
					
					price_nesia: {
						title: price_nesia,
						width: '7%',
						list:<?php echo $showPrice;?>,
						create: true,
						edit: true,
						sorting:false,
						input: function (data) {
        if (data.record) {
            return '<input type="text" id="price_nesia" name="price_nesia"  style="width:90px" value="' + data.record.price_nesia + '" /><br><br><input type="text" style="width:90px" name="price_nesiaAfter2" placeholder="'+priceAfter+'" id="price_nesiaAfter2" class="price_nesiaAfter2"  /><input type="button" value="'+calcNesia+'" onclick="calc4()" />';
        } else {
            return '<input  type="text" id="price_nesia"  name="price_nesia"   style="width:90px"  /><br><br><input type="text" name="price_nesiaAfter2" style="width:90px" placeholder="'+priceAfter+'" id="price_nesiaAfter2" class="price_nesiaAfter2" /><br><br><input type="button" value="'+calcNesia+'" onclick="calc4()" /> ';
        }
    }
					},
					processed: {
						title: processed,
						width: '3%',
						list:true,
						create: false,
						sorting:true,
						edit: true,
						options: { '1': 'כן',
								   '0': 'לא'
								   
									
						},
					
						
					}
					
				
			
				
					
					
					
					
				},
				formCreated: function (event, data) {
        data.form.find('[name=time]').mask('99:99');
		data.form.find('[name=totime]').mask('99:99');
    },
	//Validate form when it is being submitted
            formSubmitting: function (event, data) {
				if(data.form.find('[name=desc]').val()==''){alert("ERROR : "+desc);return false;}
				if(data.form.find('[name=from]').val()==''){alert("ERROR : "+from);return false;}
				if(data.form.find('[name=dest]').val()==''){alert("ERROR : "+dest);return false;}
				if(data.form.find('[name=date]').val()==''){alert("ERROR : "+date);return false;}
				if(validateSubmit($('#time').val())==false ){alert("ERROR : "+time);return false;}
				if(validateSubmit($('#totime').val())==false){alert("ERROR : "+totime);return false;}
				else{
					
					return true;
				
				}
				
            },
				rowInserted: function ( event,data) {
						
						 
						 
					   if (data.record) {
					    var rowCount = $('.jtable tr').length;
					    var mydate = new Date(data.record.date);
						var str = mydate.toString("dd-MM-yyyy");
						$(".jtable tr:eq("+(rowCount-1)+") td:eq(6)").text(str);

					  
						   if (data.record.processed==1) {
						   
						 
						  
							  $(".jtable tr:eq("+(rowCount-1)+") td:eq(0)").css("background", "#38E05D");
							  // changing first row background color
						   
						   }
						   else{
						   	 $(".jtable tr:eq("+(rowCount-1)+") td:eq(0)").css("background", "#FC3F4D");

						   
						   }
						   
					   }
					},
					 recordUpdated: function (event, data) {
					 //console.log(data.row[0].rowIndex);
					 var showPrice = "<?php echo $showPrice;?>";
					 var countIndex=12;
					 if(showPrice=='0')
						countIndex=10;
					 var new_val;
					 var mydate = new Date(data.record.date);
						var str = mydate.toString("dd-MM-yyyy");
						$(".jtable tr:eq("+(data.row[0].rowIndex)+") td:eq(6)").text(str);
						 var old_val=($(".jtable tr:eq("+(data.row[0].rowIndex)+") td:eq("+countIndex+")")[0].innerText);
						//console.log(old_val);
						var check =setInterval(function() {
						 console.log($(".jtable tr:eq("+(data.row[0].rowIndex)+") td:eq(12)"));
									new_val=($(".jtable tr:eq("+(data.row[0].rowIndex)+") td:eq("+countIndex+")")[0].innerText);
								    clearInterval(check);
									console.log(new_val);
									
									
											
											
											if(new_val=='כן'){
											
											$(".jtable tr:eq("+(data.row[0].rowIndex)+") td:eq(0)").css("background", "#38E05D");

											}
											else{
											
											 $(".jtable tr:eq("+(data.row[0].rowIndex)+") td:eq(0)").css("background", "#FC3F4D");

											}
										
						
							
					}, 500);
						
						

            },
	
			});
			
			//Load person list from server
			//$('#PeopleTableContainer').jtable('load');
			 $('#DeleteAllButton').button().click(function () {
            var $selectedRows = $('#PeopleTableContainer').jtable('selectedRows');
            $('#PeopleTableContainer').jtable('deleteRows', $selectedRows);
        });
		//Re-load records when user click 'load records' button.
        $('#LoadRecordsButton').click(function (e) {
            e.preventDefault();
			console.log($('#search').val());
			$('#filterByCust').val('');
            $('#PeopleTableContainer').jtable('load', {
                search: $('#search').val(),
                
            });
        });
		$( "#datesearch" ).blur(function(e) {
	
		$('#filterByCust').val('');
		 e.preventDefault();
 $('#PeopleTableContainer').jtable('load', {
                datesearch: $('#datesearch').val(),
                
            });
		$("#results").html('<center><img src="load.gif"  /><Br> '+loading+'</center>'); //show loading image while we process user
		
	$.ajax({
		
					url: '../openNesiotDate.php',
					type: 'POST',
					
					data: {
					d: $('#datesearch').val()
					},	
					
					success:function (data) {
					console.log("OPEN NESIOT Date");
					$('#PeopleTableContainer').jtable('load', {
					datesearch: $('#datesearch').val(),
                
            });
	$("#results").html(' '); //show loading image while we process user
				 }
			}); 

});

        //Load all records when page is first shown
      
		$('#PeopleTableContainer').jtable('option', 'pageSize', 8);
		
	function load(){
console.log('load date');	
		$('#PeopleTableContainer').jtable('load', {
				
                datesearch: '<?php echo $lastDate;?>',
                
            });
			}
			load();
			load();
			// this is for the content of the table
			var fontSize = parseInt($('.jtable').css("font-size"));
			
			fontSize = "12";
			$('.jtable').css({'font-size':fontSize});
			// this is for the headers of the table
			var fontSizetHead= parseInt($('th').css("font-size"));
		
			fontSizetHead = "12";
			$('th').css({'font-size':fontSizetHead});
		});

	</script>
<script>

$(document).keydown(function(e){
    if (e.keyCode == 113) { 
       window.open("../helpDrivers/","myNewWin","width=360,height=500,toolbar=0"); 
    }
	if (e.keyCode == 115) { 
       window.open("../helpCustomers/","myNewWin","width=330,height=500,toolbar=0"); 
    }
});
</script>


  </body>
</html>
